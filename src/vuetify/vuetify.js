import Vue from 'vue';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';
import '@fortawesome/fontawesome-free/css/all.css';
import 'material-design-icons-iconfont/dist/material-design-icons.css';
import colors from 'vuetify/lib/util/colors';

Vue.use(Vuetify);

const opts = {
    icons: {
        iconfont: 'fa' || 'md'
    },
    theme: {
        themes: {
            light: {
                primary: colors.blue.lighten1, //'#3f51b5',
                secondary: colors.green.accent4, //'#b0bec5',
                accent: '#8c9eff',
                error: colors.yellow.accent3,//avec error c'était rouge de base
            },
            dark: {
                primary: colors.blue.lighten3,
            }
        }
    }
}

export default new Vuetify(opts);